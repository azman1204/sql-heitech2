CREATE TABLE student (
 id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 NAME VARCHAR(50),
 age INT
);

TRUNCATE student;
INSERT INTO student(NAME, age) VALUES('Ali', 18), ('Abu', 19), ('John Doe', 20), ('Ahmad', 47);

CREATE TABLE lecturer (
 id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 NAME VARCHAR(50),
 age INT
);

TRUNCATE lecturer;
INSERT INTO lecturer(NAME, age) VALUES('Salman', 20), ('Salmiah', 25), ('Saloma', 30);

SELECT * FROM student WHERE age >= ALL(SELECT age FROM lecturer);
SELECT * FROM student WHERE age >= ANY(SELECT age FROM lecturer);