select * from student;
select * from lecturer;
-- carikan semua student yg umurnya > umur any lecturer
-- where age > < >= <= != <> regexp all() any()
-- student ini lebih tua dari semua lecturer
select * from student where age > all(select age from lecturer);
-- student ini lebih tua dari any lecturer
select * from student where age > any(select age from lecturer);
-- student ini lebih sama umur dgn any lecturer
select * from student where age = any(select age from lecturer);


select * from student where age in(12,20,30,47);
select * from student where age in(select age from lecturer);