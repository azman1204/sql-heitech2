create schema training2;
use training2; -- guna utk pilih database
create table employee (
	id int not null primary key auto_increment,
    name varchar(50),
    email varchar(50),
    age int
);
show tables;
desc employee;
insert into employee(name, email, age) values('Abu', 'abu@gmail.com', 30);
insert into employee(`name`, email, age) values('Ali', 'ali@gmail.com', 40);
select * from employee;

/* insert multi record in one command */ 
insert into employee(`name`, email, age) 
values('Ahmad', 'ahmad@gmail.com', 25), ('Muthusamy', 'muthu@gmail.com', 42);

-- in table employee, add a new column named post varchar(50)
alter table employee add column post varchar(50);
alter table employee add column birth_month enum('Jan', 'Feb', 'Mar');

-- update record
update employee set name = 'John Doe' where id = 1;
update employee set birth_month = 'Apr' where id = 1; -- ini error
update employee set birth_month = 'Jan' where id = 1; -- ini ok




