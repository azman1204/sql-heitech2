show processlist;
show events;
set global event_scheduler = off;
drop event test1;
delimiter //
create event test1
on schedule every 1 minute
starts current_timestamp
do
begin
	-- insert into report(item, num) values('test', 5);
    call gen_report('all');
end //
delimiter ;
alter event test1 disable;
select * from report;