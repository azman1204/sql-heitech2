-- 1. substr(string, start index bermula dr 1, length)
select substr(title, 1, 5) as sub from film;
select substr(title, 4, 3) as sub from film;

-- 2. reverse string
select reverse(title) as rvs from film;
select substr(reverse(title),1, 3) as rvs from film;

-- 3. replace(string, cari, ganti)
select replace(title, 'A', 'O') as new_title from film;

-- 4. trim(). remove space on left and right
select '  Hello  ';
select trim(' Hello ' );
select rtrim(' Hello '); -- remove space on right
select ltrim(' Hello '); -- remove space on left
select trim('0' from '00004500'); -- remove '0' kiri dan kanan
select trim('#' from '###45##'); -- remove '#' kiri dan kanan
select trim('4' from '44445');
select trim(trailing '#' from '###45##'); -- remove # yg sebelah kanan shj (belakang)
select trim(leading '#' from '###45##'); -- remove # yg sebelah kanan shj (depan)

-- 5.rpad & lpad
select rpad('123', 8, '0'); -- result: 12300000
select lpad('123', 8, '0'); -- result: 00000123


-- 6. lcase or lower, ucase or upper
SELECT LCASE(title), UCASE(title) FROM film;

-- 7. concat(). combine
select concat('Nama ', 'Saya ', 'Azman ', title) from film; 
select concat(film_id, '_', title) from film;

-- 8. left(), right()..sama mcm substring
select right(title, 10) from film;
select left(title, 10) from film;

-- 9. length().carikan filem yg paling panjang tajuknya
select title, length(title) as len from film order by len desc limit 1;
select max(len) from (select title, length(title) as len from film) as mfilm;
select title from film where length(title) = (select max(length(title)) from film);

-- 10. aes_encrypt(data, key secret) vs aes_decrypt()
select aes_encrypt('01623701111', 'secret');

select cast(aes_decrypt(enc, 'secret') as char) 
from (select aes_encrypt('01623701111', 'secret') as enc) as mydata;

-- Function Berkaitan Number
-- format number kpd beberapa decimal point
select round(10.3456, 1); -- result 10.3
select round(10.3456); -- result: 10

-- format(). sama mcm round()
select format(10.3456, 2); -- result: 10.35

-- ceil(). ceiling. convert kpd int yg paling besar
select ceil(3.75); -- result: 4

-- floor(). convert kpd int terkecil
select floor(3.75); -- result: 3

-- power()
select power(2,3); -- result: 8

-- rand(). generate random number between 0 dan 1
select rand();
-- generate random num. between 0 .. 100
select round(rand() * 100);


-- Function Berkaitan Date and Time
select payment_date from payment;
-- year(). return year dari sesuatu datetime
select year(payment_date) from payment;

-- month()
select month(payment_date) from payment;

-- kirakan jumlah jualan pd tahun 2005 mengikut bulan dan jum jualan > 5000
-- HAVING diguna bersama2 "group by" utk filter data yg di group
select month(payment_date) as mth, sum(amount) as amt
from payment 
where year(payment_date) = 2005
group by month(payment_date)
having amt > 5000
order by mth;

-- 1. date_format(date, format)
select date_format(payment_date, '%d/%m/%Y') from payment;

-- 2. time_format(datetime, format)
select time_format(payment_date, '%h.%i %p') from payment; -- result: 2.30 PM

select time_format('2023-01-01', '%h.%i %p'); -- ok, tapi haywire

-- 3. datediff(date1, date2 (lebih kecil)). kira bil hari, jam, min antara 2 date
-- masa tidak diambil kira
select datediff('2023-01-05 10:00:00', '2023-01-01 11:00:00') + 1;
select datediff('2024-01-01', '2023-01-01');

-- 4. timediff(datetime 1, datetime2)
select timediff('2023-12-19 23:00:00', '2023-12-19 20:00:00') as ot; -- result: 03:00:00
select hour(timediff('2023-12-19 23:00:00', '2023-12-19 20:00:00')) as ot; -- result: 3

-- 5. date_add(date, interval (day, month, year) vs date_sub()
select date_add('2023-12-19', interval 1 month) as new_date; -- result: 2024-01-19
select date_add('2023-12-19', interval 5 day) as new_date; -- reslt: 2024-12-24

-- Aggregation Function
-- 1. sum(), count(), min(), max(), avg()


-- Conversion Function
-- 1. cast()
select cast('0045' as decimal) * 5 as new_data;
select sqrt(cast('0036' as signed));

-- convert()
select convert('0045', decimal) / 5 as new_data;
select convert('0045', signed) / 5 as new_data;

-- str_to_date(str, format). dlm mysql default format '2023-12-19'
select date_add('2023-12-19', interval 2 day) as new_date; -- - OR / ok dlm format tapi mesti Y m d
select date_add('19/12/2023', interval 2 day) as new_date;
select date_add(str_to_date('19/12/2023', '%d/%m/%Y'), interval 2 day) as new_date;
select date_add(str_to_date('19 December 2023', '%d %M %Y'), interval 2 day) as new_date;

-- select IF(condition of data, jika true, jika false)
-- keluarkan  dari table payment,  > $7 = 'Excellent', 4 - 7 = 'Good', < 4 = 'Bad'
-- i.e 5 Good, 8 Excellent
-- if .. else
select amount, IF(amount > 7, 'Excellent', 'Bad') from payment;

-- if..else if .. else
select amount, IF(amount > 7, 'Excellent', IF(amount >= 4 and amount <= 7, 'Good', 'Bad')) 
from payment;

-- saya nak jumlah bayaran yg dibuat mengikut range : 0 - 3, 3 - 5, 5 - 7, 7 - 9, > 9
-- case .. when .. then .. else .. end
select 
	case 
		when amount >= 5 then 'Good'
		when amount < 5 then 'Bad'
    end
from payment;

create /* temporary */ table temp2 
with pmt as (select amount, 
	case 
		when amount >= 0 and amount < 3 then 'g1'
		when amount >= 3 and amount < 5 then 'g2'
        when amount >= 5 and amount < 7 then 'g3'
        when amount >= 7 and amount < 9 then 'g4'
        when amount >= 9 then 'g5'
    end as cat
from payment)
select cat, sum(amount) as amt, count(*) as cnt from pmt group by cat order by amt desc;

-- simpan data ke table dari query
create table temp select * from payment limit 5;
select * from temp;
select * from temp2;

drop table if exists temp3;
create temporary table temp3 select * from payment limit 10;
select sum(amount) from temp3;

