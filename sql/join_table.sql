select * from student;
select * from exam;

-- inner join
select * from student, exam where student.id = exam.student_id;
select * from student join exam on student.id = exam.student_id;
select * from student inner join exam on student.id = exam.student_id;

-- cari semua student sama ada ada exam atau tidak
select * from student left join exam
on student.id = exam.student_id;

-- cari semua exam dan student walaupun exam tersebut tiada student
select * from student right join exam
on student.id = exam.student_id;

-- cari data exam yg problem (tiada student)
select * from student right join exam
on student.id = exam.student_id where student.id is null;

-- student yg tiada exam
select * from student left join exam
on student.id = exam.student_id where exam.id is null;

-- union. syarat column mesti sama dari 2 atau lebih query
select * from student right join exam
on student.id = exam.student_id where student.id is null
union
select * from student left join exam
on student.id = exam.student_id where exam.id is null;

select * from student right join exam
on student.id = exam.student_id
union
select * from student left join exam
on student.id = exam.student_id;

-- more union example
select id,name from student union select id,name from lecturer;
