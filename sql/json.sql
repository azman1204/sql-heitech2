use training;
CREATE TABLE your_table (
    id INT,
    json_data JSON
);
select * from your_table;
INSERT INTO your_table VALUES (1, '{"name": "John", "age": 30, "city": "New York"}');
SELECT json_data->'$.name' AS name FROM your_table WHERE id = 1;
SELECT * FROM your_table WHERE json_data->'$.name' = 'John';
SELECT id, trim(BOTH '"' from JSON_EXTRACT(json_data, '$.name')) AS name FROM your_table;
SELECT id, JSON_UNQUOTE(JSON_EXTRACT(json_data, '$.name')) AS name FROM your_table;
