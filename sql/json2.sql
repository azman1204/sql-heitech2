alter table employee add column address json;
update employee set address = '{"no":"13A", "street":"TK 3/20", "postcode":42000}' 
where id = 1;

update employee set address = '{"no":"14A", "street":"TK 3/21", "postcode":43000}' 
where id = 2;

-- Javascript Object Notation
select address, children from employee;
select address->'$.postcode' from employee;
select * from employee where address->'$.postcode' = 43000;
select json_extract(address, '$.street') from employee;
select json_unquote(json_extract(address, '$.street')) as street from employee;
-- update json data
update employee set address = json_set(address, '$.postcode', 9000) where id = 1;

-- update employee simpan maklumat children json (name, age), anak boleh multiple
alter table employee add column children json;
update employee set children = '[{"name": "Abu", "age": 5}, {"name": "Zarina", "age": 10}]' 
where id = 1;
-- query dan paparkan nama anak kedua, result patut "Jane"
select children->'$.name' from employee; -- not working
select children->'$[1].name' from employee; -- ok
select json_extract(children, '$[1].name') from employee;
-- delete anak ke 2 dari children column
update employee set children = json_remove(children, '$[1]') where id = 1;

select children->'$[*].name' from employee;

update employee 
set children = json_array_append(children, '$', cast('{"name":"Salim", "age":14}' AS JSON)) 
where id = 1;








