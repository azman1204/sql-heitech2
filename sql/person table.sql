create table person (
  id varchar(10) primary key,
  first_name varchar(50),
  last_name varchar(50),
  age int
);
select * from person;
truncate person;