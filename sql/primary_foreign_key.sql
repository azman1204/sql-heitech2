-- How to create primary key and foreign key

DROP TABLE orders;
CREATE TABLE orders (
    order_id INT NOT NULL AUTO_INCREMENT,
    order_no INT NOT NULL,
    person_id INT,
    PRIMARY KEY (order_id),
    FOREIGN KEY (person_id) REFERENCES persons(person_id)
);

INSERT INTO orders(order_no, person_id) VALUES(100, 1);
INSERT INTO orders(order_no, person_id) VALUES(100, 10);


CREATE TABLE persons (
  person_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  NAME VARCHAR(50)
);


INSERT INTO persons(NAME) VALUES('John Doe'), ('Ahmad');