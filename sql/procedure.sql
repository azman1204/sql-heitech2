create table report (
  id int not null auto_increment,
  item varchar(50),
  num int,
  primary key(id)
);

drop procedure gen_report;
delimiter $$
-- IN or OUT. cat = student | lecturer | all
create procedure gen_report(IN cat varchar(10))
begin
	declare num_student int default 0;
    declare num_lecturer int default 0;
    
    truncate report;
	if cat = 'student' then
		select count(*) into num_student from student;
		insert into report(item, num) values('student', num_student);
	elseif cat = 'lecturer' then
		select count(*) into num_lecturer from lecturer;
		insert into report(item, num) values('lecturer', num_lecturer);
	else
		select count(*) into num_student from student;
		insert into report(item, num) values('student', num_student);
        select count(*) into num_lecturer from lecturer;
		insert into report(item, num) values('lecturer', num_lecturer);
    end if;
end $$
delimiter ;

call gen_report('student');
call gen_report('lecturer');
call gen_report('all');
select * from report;

