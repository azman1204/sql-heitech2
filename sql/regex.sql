select * from film where title like 'A%A';

-- Regular Expression
-- title start with 'A'
select title from film where title regexp '^A'; 

-- title start with 'A' and End with 'A'
select title from film where title regexp '^A.+A$';

-- title start with 'A' OR start with 'B'
select title from film where title regexp '^[AB]';

-- title with 'LOVE'
select title from film where title regexp 'LOVE';
select title from film where title LIKE '%LOVE%';

-- tentukan no ic ini valid atau tidak 123456-12-1234
-- rules: 1. 12 char  2. depan 6 - tgh 2 -belakang 4 3. no tengah x boleh < 16
-- jika IC valid, return Valid sebalik Not Valid
select if(is_valid = 1, 'Valid', 'Not Valid') from
(select regexp_like('123456-12-1234', '^[0-9]{6}-[01][0-9]-[0-9]{4}$') as is_valid) as rx;

select regexp_like('saya suka makan', 'saya');

-- carikan tajuk film yg ada dua 'F' yg berturutan pd perkataan yg pertama. i.e AFFAIR
select * from film where title regexp 'FF.+ .+$';

