select * from film;
-- cari semua film yg rental duration > 7 hari
select * from film where rental_duration >= 7;

-- cari semua nama filem dan nama pelakon bagi filem yg ada perkataan 'LOVE'
select f.title, a.first_name
from film f, film_actor fa, actor a
where f.film_id = fa.film_id and a.actor_id = fa.actor_id
and f.title like '%love%' order by f.title;

-- cari semua film yg panjangnya antara 50 ke 80 min
select title, length from film where length between 50 and 80;
select title, length from film where length >= 50 and length <= 80;

-- group by. cari bil film mengikut rating
select rating, count(*) as cnt from film group by rating;

-- cari bil film mengikut rating dgn tambahan colum (pct)%
select 'ABC', 5*5;
select rating, count(*) as cnt, (count(*) / (select count(*) from film)) * 100 as pct
from film group by rating;

select rating, count(*) as cnt, format((count(*) / (select count(*) from film)) * 100, 1) as pct
from film group by rating;

select rating, count(*) as cnt, round((count(*) / (select count(*) from film)) * 100) as pct
from film group by rating;


select rating, round(pct) from 
(select rating, count(*) as cnt, (count(*) / (select count(*) from film) * 100) as pct
from film group by rating) as q1;

-- CTE - sama konsep seperti sub-query (query on query)
with mycte as (
	select rating, count(*) as cnt from film group by rating
)
select rating, cnt + 10 from mycte;


