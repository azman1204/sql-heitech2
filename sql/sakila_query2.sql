-- top 5 most number of rental by customer
select * from rental;
select customer_id, count(*) as cnt 
from rental group by customer_id
order by cnt desc
limit 5;


with top_customer AS (
	select customer_id, count(*) as cnt 
	from rental group by customer_id
	order by cnt desc limit 5
)
select a.first_name, b.cnt 
from customer a, top_customer b
where a.customer_id = b.customer_id;