create table state (
  id int not null primary key auto_increment,
  state_name varchar(50),
  state_code varchar(20)
);

select * from state;
truncate state;