/* ---------------------------------------------
This script to describe LEFT, RIGHT and JOIN statement
   ---------------------------------------------*/

CREATE TABLE exam(
 id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 student_id INT NOT NULL,
 gpa FLOAT 
);

INSERT INTO exam(student_id, gpa) VALUES(1, 3.5), (2, 3.4), (100, 2.9), (101, 3.0);

SELECT * FROM student JOIN exam ON student.id = exam.student_id;
SELECT * FROM student LEFT JOIN exam ON student.id = exam.student_id;
SELECT * FROM student RIGHT JOIN exam ON student.id = exam.student_id;

-- student yg tidak ada exam
SELECT * FROM student LEFT JOIN exam ON student.id = exam.student_id WHERE exam.id IS NULL;

-- exam yg tiada student
SELECT * FROM student RIGHT JOIN exam ON student.id = exam.student_id WHERE student.id IS NULL;