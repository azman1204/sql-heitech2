-- https://gitlab.com/azman1204/sql-heitech2
-- sql/student_exam.sql
CREATE TABLE exam(
 id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 student_id INT NOT NULL,
 gpa FLOAT 
);

INSERT INTO exam(student_id, gpa) VALUES(1, 3.5), (2, 3.4), (100, 2.9), (101, 3.0);
select * from exam;

-- sql/any_all.sql
CREATE TABLE student (
 id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 NAME VARCHAR(50),
 age INT
);

TRUNCATE student;
INSERT INTO student(NAME, age) VALUES('Ali', 18), ('Abu', 19), ('John Doe', 20), ('Ahmad', 47);

CREATE TABLE lecturer (
 id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 NAME VARCHAR(50),
 age INT
);

TRUNCATE lecturer;
INSERT INTO lecturer(NAME, age) VALUES('Salman', 20), ('Salmiah', 25), ('Saloma', 30);



