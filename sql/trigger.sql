table student_hist (
  id int not null primary key auto_increment,
  name varchar(50),
  age int,
  updated_at datetime
);

-- old = previous record
drop trigger on_update_student;
create trigger on_update_student before update on student for each row
insert into student_hist(name, age, updated_at, action) 
values(old.name, old.age, current_timestamp(), 'UPDATE') ;

select * from student;
update student set age = 25 where id = 200;
select * from student_hist;

-- modify table student_hist tambah column 'action varchar(50)' - UPDATE, DELETE
-- create trigger, on delete simpan history delete
alter table student_hist add column action varchar(50);

create trigger on_delete_student after delete on student for each row
insert into student_hist(name, age, updated_at, action) 
values(old.name, old.age, current_timestamp(), 'DELETE');

delete from student where id = 1;

-- jika insert / update / delete tidak berjaya.. maka trigger juga tidak run
create trigger on_insert_student after insert on student for each row
insert into student_hist(name, age, updated_at, action) 
values(new.name, new.age, current_timestamp(), 'INSERT');

insert into student(name, age) values('Jamal', 56);
select * from student_hist;
