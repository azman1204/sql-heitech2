set global log_bin_trust_function_creators = 1; -- allow any function creation
drop function tambah;
delimiter $$
create function tambah (no1 int, no2 int) returns int
begin
	declare result int;
    set result = no1 + no2;
    return result;
end $$
delimiter ;

select tambah(3,4) as no1, tambah(5, 5) as no2;
-- create satu UDF, yg return average bayaran dari table payment

delimiter //
create function get_average_payment() returns float
begin
	declare average float;
	SELECT AVG(amount) into average from payment;
    return average;
end //
delimiter ;

select get_average_payment();

-- create kan UDF, terima satu parameter customer_id, return nama customer (first dan last name)
drop function if exists get_customer_name;
delimiter //
create function get_customer_name(cid int) returns varchar(100)
begin
	declare customer_name varchar(100);
	SELECT concat(first_name, ' ', last_name) into customer_name from customer
    where customer_id = cid;
    return customer_name;
end //
delimiter ;
select get_customer_name(2);
