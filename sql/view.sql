-- view. create satu view bernama "payment_summary"
-- column: customer_id, customer_name, tot_payment

create or replace view payment_summary as
select c.address_id, c.first_name, c.customer_id, sum(p.amount) as tot_payment
from payment p, customer c
where p.customer_id = c.customer_id
group by c.first_name, c.customer_id, c.address_id;

select * from payment_summary;
-- kirakan semua jumlah jualan bg customer di bandar 'Abu Dhabi'
-- table terlibat payment_summary, address, city
select sum(a.tot_payment) as tot
from payment_summary a, address b, city c
where a.address_id = b.address_id and
	  b.city_id = c.city_id and
      c.city = 'Abu Dhabi';

select * from actor_info;
select * from customer_list;

-- join table using join statement
create or replace view senarai_pelanggan as
select ct.customer_id, ct.first_name, a.address, c.city
from customer ct join address a on ct.address_id = a.address_id
				 join city c on a.city_id = c.city_id;

select * from senarai_pelanggan;
select * from payment_summary;
-- join 2 view
select * 
from payment_summary a, senarai_pelanggan b
where a.customer_id = b.customer_id;

-- create a view dari join 2 view sediada
create or replace view customer_payment_summary as
select b.first_name, b.city, a.tot_payment
from payment_summary a, senarai_pelanggan b
where a.customer_id = b.customer_id;

select city, sum(tot_payment) from customer_payment_summary group by city;







